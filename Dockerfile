FROM node:alpine

RUN apk add --no-cache openssh-client git

RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

RUN --mount=type=ssh git clone git@gitlab.com:consilia/simpleweb.git 

WORKDIR ./simpleweb

RUN npm install

CMD ["npm", "start"]
